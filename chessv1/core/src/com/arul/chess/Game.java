package com.arul.chess;

import javax.swing.JOptionPane;

import com.badlogic.gdx.Gdx;

import pieces.*;

public class Game {

	public Piece[][] gameBoard;

	public colours currentTurn = colours.WHITE;

	public boolean pieceSelected = false;
	public int[] selectedPieceCoords = new int[2];
	Piece temp;

	com.arul.chess.Player p1;
	com.arul.chess.Player p2;

	int saveID;
	String currentSave;
	private String whitePlayer;

	/**
	 * new game constructor - pieces initialized to default chess position
	 */
	public Game(com.arul.chess.Player _p1, com.arul.chess.Player _p2, int _saveID) {
		p1 = _p1;
		p2 = _p2;
		saveID = _saveID;

		gameBoard = new Piece[8][8];

		whitePlayer = p1.getUsername();

		for (int i = 0; i < 8; i++) {
			gameBoard[i][1] = new Pawn(colours.WHITE, new int[] { i, 1 });

		}
		gameBoard[0][0] = new Rook(colours.WHITE, new int[] { 0, 0 });
		gameBoard[7][0] = new Rook(colours.WHITE, new int[] { 7, 0 });

		gameBoard[2][0] = new Bishop(colours.WHITE, new int[] { 2, 0 });
		gameBoard[5][0] = new Bishop(colours.WHITE, new int[] { 5, 0 });

		gameBoard[1][0] = new Knight(colours.WHITE, new int[] { 1, 0 });
		gameBoard[6][0] = new Knight(colours.WHITE, new int[] { 6, 0 });

		gameBoard[3][0] = new Queen(colours.WHITE, new int[] { 3, 0 });

		gameBoard[4][0] = new King(colours.WHITE, new int[] { 4, 0 });

		for (int i = 0; i < 8; i++) {
			gameBoard[i][6] = new Pawn(colours.BLACK, new int[] { i, 6 });
		}

		gameBoard[0][7] = new Rook(colours.BLACK, new int[] { 0, 7 });
		gameBoard[7][7] = new Rook(colours.BLACK, new int[] { 7, 7 });

		gameBoard[2][7] = new Bishop(colours.BLACK, new int[] { 2, 7 });
		gameBoard[5][7] = new Bishop(colours.BLACK, new int[] { 5, 7 });

		gameBoard[1][7] = new Knight(colours.BLACK, new int[] { 1, 7 });
		gameBoard[6][7] = new Knight(colours.BLACK, new int[] { 6, 7 });

		gameBoard[3][7] = new Queen(colours.BLACK, new int[] { 3, 7 });

		gameBoard[4][7] = new King(colours.BLACK, new int[] { 4, 7 });

		currentSave = "rnbqkbnr/pppppppp/eeeeeeee/eeeeeeee/eeeeeeee/eeeeeeee/PPPPPPPP/RNBQKBNR w" + " "
				+ p1.getUsername();
	}

	/**
	 * save game constructor
	 *
	 * @param p1
	 * @param p2
	 * @param saveID
	 * @param save
	 */
	public Game(Player _p1, Player _p2, int _saveID, String _save) {
		p1 = _p1;
		p2 = _p2;
		saveID = _saveID;

		gameBoard = new Piece[8][8];
		currentSave = _save;
		StringToBoard(_save);
	}

	public void deletePiece(int x, int y) {
		gameBoard[x][y] = null;
		MainChessGUI.spriteBoard[x][y] = null;
	}

	public void movePiece(int o_x, int o_y, int n_x, int n_y) {
		MainChessGUI.spriteBoard[o_x][o_y].setPositionFromBoard(n_x, n_y);
		gameBoard[o_x][o_y].movePiece(new int[] { n_x, n_y });
		gameBoard[n_x][n_y] = gameBoard[o_x][o_y];
		MainChessGUI.spriteBoard[n_x][n_y] = MainChessGUI.spriteBoard[o_x][o_y];
		deletePiece(o_x, o_y);
	}

	public void movePiece(int[] o, int[] n) {
		movePiece(o[0], o[1], n[0], n[1]);
	}

	public void movePieceBackend(int[] o, int[] n) {
		movePieceBackend(o[0], o[1], n[0], n[1]);
	}

	public void movePieceBackend(int o_x, int o_y, int n_x, int n_y) {
		temp = gameBoard[n_x][n_y];
		gameBoard[n_x][n_y] = gameBoard[o_x][o_y];
		// gameBoard[o_x][o_y] = temp;
		gameBoard[o_x][o_y] = null;
	}

	public void undoBackendMove(int o_x, int o_y, int n_x, int n_y) {
		gameBoard[o_x][o_y] = gameBoard[n_x][n_y];
		gameBoard[n_x][n_y] = temp;
	}

	/**
	 * wrapper method(array to ints)
	 * 
	 * @param o
	 *            - original coords as array
	 * @param n
	 *            - new coords as array
	 */
	public void undoBackendMove(int[] o, int n[]) {
		undoBackendMove(o[0], o[1], n[0], n[1]);
	}

	/**
	 * make the move happen, on backend and frontend
	 * 
	 * @param arrayTarget
	 *            - the new position of the piece selected
	 */
	public void executeMove(int[] arrayTarget) {
		// pieceSelected = false;
		MainChessGUI.spriteBoard[selectedPieceCoords[0]][selectedPieceCoords[1]].isSelected = false;

		movePiece(selectedPieceCoords[0], selectedPieceCoords[1], arrayTarget[0], arrayTarget[1]);
		currentTurn = colours.changeColour(currentTurn);
		// System.out.println("It is now: " + currentTurn + "'s turn");
		pieceSelected = false;

		if (isCheckMate(currentTurn)) {
			JOptionPane.showMessageDialog(null, "check mate! " + colours.changeColour(currentTurn) + " wins!");
			System.out.println("check mate! " + colours.changeColour(currentTurn) + " wins!");
			System.out.println("exiting soon");
			end();
			Gdx.app.exit();
			System.exit(0);
		} else if (isStaleMate(currentTurn)) {
			JOptionPane.showMessageDialog(null, "stalemate!");
			System.out.println("stalemate!");
			System.out.println("exiting soon");
			p1.draw();
			p2.draw();
			deleteSave();
			Gdx.app.exit();
			// System.exit(0);
		} else if (isCheck(currentTurn)) {
			JOptionPane.showMessageDialog(null, "You are in Check!");
			System.out.println("you are in check!");
		}
	}

	/**
	 * end the game, and increment player scores accordingly.
	 */
	public void end() {
		if (currentTurn == colours.WHITE) {
			// black wins
			if (p1.getUsername().equals(whitePlayer)) {
				// p1 lost
				p1.lose();
				p2.win();
			} else {
				// p1 won
				p1.win();
				p2.lose();
			}
		} else {
			if (p1.getUsername().equals(whitePlayer)) {
				// p1 won
				p1.win();
				p2.lose();
			} else {
				// p1 lost
				p1.lose();
				p2.win();
			}
		}
		deleteSave();
	}

	/**
	 * delete completed game from database
	 */
	public void deleteSave() {
		try {
			StorageManager stm = new StorageManager("storage.accdb");
			String query = "DELETE * FROM tblSaves WHERE ID = " + saveID;
			stm.update(query);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * Master Validation Algorithm
	 *
	 * @param fromCoords
	 *            coordinates of piece being moved
	 * @param toCoords
	 *            destination coordinates
	 *
	 * @return true if move is valid under chess rules, false if not
	 */
	public boolean validateMove(int[] fromCoords, int[] toCoords) {
		// check colour
		if (gameBoard[fromCoords[0]][fromCoords[1]] == null) {
			return false;
		}
		if (!(currentTurn == gameBoard[fromCoords[0]][fromCoords[1]].getColour())) {
			// System.out.println("colour not same");
			return false;
		}

		// piece to move coords are stored in selectedPieceCoords
		// if the move isn't valid return false straight away
		if (!gameBoard[fromCoords[0]][fromCoords[1]].isValidMovement(toCoords)) {
			// System.out.println("movement not valid");
			return false;
		}

		// if current colour was just put into check, currentcolour's turn needs to get
		// him out of check
		// move piece backend, and check for check
		if (isCheck(currentTurn)) {
			// System.out.println("check things");
			movePieceBackend(fromCoords, toCoords);
			boolean stillCheck = isCheck(currentTurn);
			if (stillCheck) {//
				// move piece back
				// System.out.println("we are still in check?");
				undoBackendMove(fromCoords, toCoords);
				// movePieceBackend(toCoords, fromCoords);
				return false;
			}
			// no longer check - probably broke something
			// movePieceBackend(toCoords, fromCoords);
			undoBackendMove(fromCoords, toCoords);
			return true;
		}
		// perhaps make sure the player cant move into check either
		movePieceBackend(fromCoords, toCoords);
		if (isCheck(currentTurn)) {
			undoBackendMove(fromCoords, toCoords);
			return false;
		}
		undoBackendMove(fromCoords, toCoords);

		return true;
	}

	/**
	 * find a king
	 * 
	 * @param color
	 *            - the colour's whose king is being found
	 * @return - the coordinates of the king
	 */
	public int[] findKing(colours color) {
		// find current colour
		// need kings position
		int[] kingPos = new int[2];

		// find white king pos; loop through whole board
		for (int i = 0; i < gameBoard.length; i++) {
			for (int j = 0; j < gameBoard.length; j++) {
				if (gameBoard[i][j] != null) {
					if (gameBoard[i][j].getColour() == color && gameBoard[i][j].getPieceType() == pieces.KING) {
						kingPos[0] = i;
						kingPos[1] = j;
					}
				}
			}
		}
		return kingPos;
	}

	/**
	 * Check Algorithm
	 *
	 * @param color
	 *            - the colour to test check against
	 *
	 * @return true if input colour is in check, false if not
	 */
	public boolean isCheck(colours color) {
		int[] kingPos = findKing(color);
		// found color king pos - see if any oppcolor piece can attack that pos
		colours oppColour = colours.changeColour(color);
		// boolean canAttack = false;
		for (int i = 0; i < gameBoard.length; i++) {
			for (int j = 0; j < gameBoard.length; j++) {
				if (gameBoard[i][j] != null) {
					if (gameBoard[i][j].getColour() == oppColour) {
						if (gameBoard[i][j].isValidMovement(kingPos)) {
							return true;
						} else {
							continue;
						}
					}
				}
			}
		}
		return false;
	}

	/**
	 * brute force see if its check mate
	 * 
	 * @param color
	 *            - the colour being checked for checkmate
	 * @return - true if color in checkmate, false if not
	 */
	public boolean isCheckMate(colours color) {
		// king has to be in check to be in checkmate, check that first
		if (!isCheck(color)) {
			return false;
		}
		// brute force every move to every position, if a move get the player out of
		// check, the player is not in checkmate
		for (Piece[] a : gameBoard) {
			for (Piece p : a) {
				if (p != null) {
					if (p.getColour() == color) {
						for (int i = 0; i < 8; i++) {
							for (int j = 0; j < 8; j++) {
								if (validateMove(p.getCoords(), new int[] { i, j })) {
									// System.out.println("found valid move:" + p.getCoords()[0] + ":" +
									// p.getCoords()[1]
									// + " to " + i + ":" + j);
									return false;
								}
							}
						}
					}
				}

			}
		}
		return true;
	}

	/**
	 * brute force see if its stale mate
	 * 
	 * @param color
	 *            - the colour being checked for stalemate
	 * @return - true if color in stalemate, false if not
	 */
	public boolean isStaleMate(colours color) {
		if (isCheck(color)) {
			return false;
		} else {
			for (Piece[] a : gameBoard) {
				for (Piece p : a) {
					if (p != null) {
						if (p.getColour() == color) {
							for (int i = 0; i < 8; i++) {
								for (int j = 0; j < 8; j++) {
									if (validateMove(p.getCoords(), new int[] { i, j })) {
										// System.out.println("found valid move:" + p.getCoords()[0] + ":" +
										// p.getCoords()[1]
										// + " to " + i + ":" + j);
										return false;
									}
								}
							}
						}
					}

				}
			}
			return true;
		}
	}

	/**
	 * update the database with the current board position
	 */
	public void save() {
		String newSave = BoardToString();
		// System.out.println("new save is" + newSave);
		try {
			StorageManager stm = new StorageManager("storage.accdb");
			String query = "UPDATE tblSaves SET Save = '" + newSave + "' WHERE ID = " + saveID;
			stm.update(query);
		} catch (Exception e) {
			System.out.println(e);
		}
		currentSave = newSave;
	}

	/**
	 * convert string from db to board array - backend
	 * 
	 * @param save
	 *            - string from db
	 */
	public void StringToBoard(String save) {
		String[] data = save.split(" ");
		String[] board = data[0].split("/");
		whitePlayer = data[2];
		// System.out.println();
		// System.out.println("coverting string to board");
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				char ch = board[i].charAt(j);
				int x = 7 - i;
				if (ch == 'e') {
					// no piece
				} else if (Character.isUpperCase(ch)) {
					// white piece
					switch (ch) {
					case 'P':
						gameBoard[j][x] = new Pawn(colours.WHITE, new int[] { j, x });
						break;
					case 'N':
						gameBoard[j][x] = new Knight(colours.WHITE, new int[] { j, x });
						break;
					case 'B':
						gameBoard[j][x] = new Bishop(colours.WHITE, new int[] { j, x });
						break;
					case 'R':
						gameBoard[j][x] = new Rook(colours.WHITE, new int[] { j, x });
						break;
					case 'Q':
						gameBoard[j][x] = new Queen(colours.WHITE, new int[] { j, x });
						break;
					case 'K':
						gameBoard[j][x] = new King(colours.WHITE, new int[] { j, x });
						break;
					default:
						System.out.println("wut");
						break;
					}
				} else {
					switch (ch) {
					case 'p':
						gameBoard[j][x] = new Pawn(colours.BLACK, new int[] { j, x });
						break;
					case 'n':
						gameBoard[j][x] = new Knight(colours.BLACK, new int[] { j, x });
						break;
					case 'b':
						gameBoard[j][x] = new Bishop(colours.BLACK, new int[] { j, x });
						break;
					case 'r':
						gameBoard[j][x] = new Rook(colours.BLACK, new int[] { j, x });
						break;
					case 'q':
						gameBoard[j][x] = new Queen(colours.BLACK, new int[] { j, x });
						break;
					case 'k':
						gameBoard[j][x] = new King(colours.BLACK, new int[] { j, x });
						break;
					default:
						System.out.println("wut");
						break;
					}
				}
			}
		}
		switch (data[1]) {
		case "w":
			currentTurn = colours.WHITE;
			break;
		case "b":
			currentTurn = colours.BLACK;
			break;
		default:
			System.out.println("wtf");
			break;
		}

	}

	/**
	 * convert current backend board to string
	 * 
	 * @return - the entire game as a string
	 */
	public String BoardToString() {
		// okay let's think now
		// white's perspective, start from top
		String result = "";
		for (int i = 7; i >= 0; i--) {
			for (int j = 0; j < 8; j++) {
				if (gameBoard[j][i] != null) {
					if (gameBoard[j][i].getColour() == colours.WHITE) {
						switch (gameBoard[j][i].getPieceType()) {
						case BISHOP:
							result += "B";
							break;
						case KING:
							result += "K";
							break;
						case KNIGHT:
							result += "N";
							break;
						case PAWN:
							result += "P";
							break;
						case QUEEN:
							result += "Q";
							break;
						case ROOK:
							result += "R";
							break;
						default:
							break;

						}
					} else {
						switch (gameBoard[j][i].getPieceType()) {
						case BISHOP:
							result += "b";
							break;
						case KING:
							result += "k";
							break;
						case KNIGHT:
							result += "n";
							break;
						case PAWN:
							result += "p";
							break;
						case QUEEN:
							result += "q";
							break;
						case ROOK:
							result += "r";
							break;
						default:
							break;

						}
					}
				} else {
					result += "e";
				}

			}
			result += "/";
		}
		switch (currentTurn) {
		case BLACK:
			result += " b";
			break;
		case WHITE:
			result += " w";
			break;
		default:
			break;

		}
		result += " " + whitePlayer;
		return result;
	}
}
