/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pieces;

import com.arul.chess.MainChessGUI;

/**
 *
 * @author agraru13
 */
public class Knight extends Piece {

    public Knight(colours colour, int[] coords) {
        super(pieces.KNIGHT, colour, coords);
    }

    @Override
    public boolean isValidMovement(int[] targetCoords) {
        try {
            if (MainChessGUI.m_game.gameBoard[targetCoords[0]][targetCoords[1]].pieceColour == this.pieceColour) {
                return false;
            }
        } catch (NullPointerException e) {
            System.out.println("can move");
        }
        if (Math.abs(this.coords[0] - targetCoords[0]) == 1) {
            if (Math.abs(this.coords[1] - targetCoords[1]) == 2) {
                return true;
            }
        }
        if (Math.abs(this.coords[1] - targetCoords[1]) == 1) {
            if (Math.abs(this.coords[0] - targetCoords[0]) == 2) {
                return true;
            }
        }
        return false;
    }

}
