/*
 * + * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arul.chess;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 *
 * @author agraru13
 */
public class PieceSprite extends Sprite{

    public boolean isSelected = false;

    public PieceSprite(Texture tex){
        super(tex);
    }

    @Override //why does this exist?
    public void setPosition(float x, float y){
        super.setPosition(x, y); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void setPositionFromBoard(float x, float y) {
    	setPosition(x*50, y*50);
    }

    
}
