/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pieces;

import com.arul.chess.MainChessGUI;

/**
 *
 * @author agraru13
 */
public class King extends Piece{

	boolean hasCastled;
    public King(colours colour, int[] coords) {
        super(pieces.KING, colour, coords);
		hasCastled = false;
    }

    @Override
    public boolean isValidMovement(int[] targetCoords) {
        try {
            if (MainChessGUI.m_game.gameBoard[targetCoords[0]][targetCoords[1]].pieceColour == this.pieceColour) {
            	System.out.println("1");
                return false;
            }
        } catch (NullPointerException e) {
            System.out.println("can move");
        }
		if(checkCastle(targetCoords)){
			hasCastled = true;
			return true;
		}
        if(Math.abs(targetCoords[0] - this.coords[0]) <= 1 && Math.abs(targetCoords[1] - this.coords[1]) <= 1){
            hasCastled = true;
			return true;
        }else{
        	System.out.println("current pos:" + this.coords[0] + ";" + this.coords[1]);
            System.out.println("target pos:" + targetCoords[0] + ";" + targetCoords[1]);
        	System.out.println("2");
            return false;
        }
    }

	public boolean checkCastle(int[] toCoords){
		if(hasCastled){
			return false;
		}
		//y value must be consistent
		if(coords[1] != toCoords[1]){
			return false;
		}
		//the position must either be +2 or -2
		if(toCoords[0] - coords[0] == 2){
			if(MainChessGUI.m_game.gameBoard[toCoords[0] + 1][toCoords[1]].getPieceType() == pieces.ROOK){
				//check if pieces in between are null
				if(MainChessGUI.m_game.gameBoard[5][toCoords[1]] == null && MainChessGUI.m_game.gameBoard[6][toCoords[1]] == null){
					//we can castle
					MainChessGUI.m_game.movePiece(toCoords[0]+1, toCoords[1], 5, toCoords[1]);
					this.coords = toCoords;
					hasCastled = true;
					return true;
				}
			}
		}
		if(toCoords[0] - coords[0] == -2) {
			if(MainChessGUI.m_game.gameBoard[toCoords[0] - 2][toCoords[1]].getPieceType() == pieces.ROOK){
				if(MainChessGUI.m_game.gameBoard[1][toCoords[1]] == null && MainChessGUI.m_game.gameBoard[2][toCoords[1]] == null && MainChessGUI.m_game.gameBoard[3][toCoords[1]] == null){
					//we can castle
					MainChessGUI.m_game.movePiece(toCoords[0]-2, toCoords[1], 3, toCoords[1]);
					this.coords = toCoords;
					hasCastled = true;
					return true;
				}
			}
		}
		return false;
	}
    
}
