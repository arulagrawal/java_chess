package com.arul.chess;


import com.arul.chess.PieceSprite;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import pieces.*;

public class MainChessGUI extends ApplicationAdapter implements InputProcessor, Screen {

	private OrthographicCamera camera;
	SpriteBatch batch;
	Texture img;
	Texture boardImg;

	Texture saveTex;
	Texture exitTex;
	Sprite saveButton;
	Sprite exitButton;

	Sprite board;
	public static com.arul.chess.Game m_game;
	public static PieceSprite[][] spriteBoard;

	Texture whitePawnTex;
	Texture whiteRookTex;
	Texture whiteBishopTex;
	Texture whiteKnightTex;
	Texture whiteQueenTex;
	Texture whiteKingTex;
	// git test comment
	Texture blackPawnTex;
	Texture blackRookTex;
	Texture blackBishopTex;
	Texture blackKnightTex;
	Texture blackQueenTex;
	Texture blackKingTex;

	// PawnPieceSprite[] pawns = new PawnPieceSprite[8];
	public MainChessGUI(com.arul.chess.Game _game) {
		super();
		m_game = _game;

	}

	public MainChessGUI() {
		super();
	}

	public void createTextures() {
		whitePawnTex = new Texture("pieces/Chess_plt60.png");
		whiteRookTex = new Texture("pieces/Chess_rlt60.png");
		whiteBishopTex = new Texture("pieces/Chess_blt60.png");
		whiteKnightTex = new Texture("pieces/Chess_nlt60.png");
		whiteQueenTex = new Texture("pieces/Chess_qlt60.png");
		whiteKingTex = new Texture("pieces/Chess_klt60.png");

		blackPawnTex = new Texture("pieces/Chess_pdt60.png");
		blackRookTex = new Texture("pieces/Chess_rdt60.png");
		blackBishopTex = new Texture("pieces/Chess_bdt60.png");
		blackKnightTex = new Texture("pieces/Chess_ndt60.png");
		blackQueenTex = new Texture("pieces/Chess_qdt60.png");
		blackKingTex = new Texture("pieces/Chess_kdt60.png");
	}

	public void createSpritesFromBoard() {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (m_game.gameBoard[i][j] != null) {
					if (m_game.gameBoard[i][j].getColour() == colours.WHITE) {
						switch (m_game.gameBoard[i][j].getPieceType()) {
						case BISHOP:
							spriteBoard[i][j] = new PieceSprite(whiteBishopTex);
							break;
						case KING:
							spriteBoard[i][j] = new PieceSprite(whiteKingTex);
							break;
						case KNIGHT:
							spriteBoard[i][j] = new PieceSprite(whiteKnightTex);
							break;
						case PAWN:
							spriteBoard[i][j] = new PieceSprite(whitePawnTex);
							break;
						case QUEEN:
							spriteBoard[i][j] = new PieceSprite(whiteQueenTex);
							break;
						case ROOK:
							spriteBoard[i][j] = new PieceSprite(whiteRookTex);
							break;
						default:
							System.out.println("what piece is this?????");
							break;

						}
					} else {
						// piece is black
						switch (m_game.gameBoard[i][j].getPieceType()) {
						case BISHOP:
							spriteBoard[i][j] = new PieceSprite(blackBishopTex);
							break;
						case KING:
							spriteBoard[i][j] = new PieceSprite(blackKingTex);
							break;
						case KNIGHT:
							spriteBoard[i][j] = new PieceSprite(blackKnightTex);
							break;
						case PAWN:
							spriteBoard[i][j] = new PieceSprite(blackPawnTex);
							break;
						case QUEEN:
							spriteBoard[i][j] = new PieceSprite(blackQueenTex);
							break;
						case ROOK:
							spriteBoard[i][j] = new PieceSprite(blackRookTex);
							break;
						default:
							System.out.println("what piece is this?????");
							break;

						}

					}
					spriteBoard[i][j].setPosition(i * 50, j * 50);
				}
			}
		}
	}

	public void create() {
		spriteBoard = new PieceSprite[8][8];

		createTextures();
		createSpritesFromBoard();

		fixSpriteSizes();

		camera = new OrthographicCamera();
		camera.setToOrtho(false, 600, 400);
		batch = new SpriteBatch();
		// img = new Texture("badlogic.jpg");
		boardImg = new Texture("chessboard.png");
		saveTex = new Texture("save.png");
		exitTex = new Texture("exit.png");

		board = new Sprite(boardImg, 0, 0, 400, 400);

		saveButton = new Sprite(saveTex, 400, 200, 200, 200);
		saveButton.setPosition(400, 200);
		exitButton = new Sprite(exitTex, 400, 0, 200, 200);
		exitButton.setPosition(400, 0);

		Gdx.input.setInputProcessor(this);
	}

	public void render() {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		board.draw(batch);
		// saveButton.draw(batch);
		// exitButton.draw(batch);
		batch.draw(saveTex, 400, 200);
		batch.draw(exitTex, 400, 0);
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (spriteBoard[i][j] != null) {
					spriteBoard[i][j].draw(batch);
				}

			}
		}
		batch.end();
	}

	private void fixSpriteSizes() {
		for (PieceSprite[] spriteArray : spriteBoard) {
			for (PieceSprite sprite : spriteArray) {
				if (sprite != null) {
					sprite.setSize(50f, 50f);
				}
			}
		}
	}

	@Override
	public void dispose() {
		batch.dispose();
		img.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Keys.C) {
			System.out.println("Is " + m_game.currentTurn + " in check? " + m_game.isCheck(m_game.currentTurn));
		}

		if (keycode == Keys.M) {
			System.out
					.println("Is " + m_game.currentTurn + " in checkMate!? " + m_game.isCheckMate(m_game.currentTurn));
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	public int[] roundCoords(float x, float y) {
		int newX = (int) (x - 50 + 49) / 50 * 50;
		int newY = (int) (y - 50 + 49) / 50 * 50;
		return new int[] { newX, newY };
	}

	public int[] boardToArray(int[] input) {
		return new int[] { input[0] / 50, input[1] / 50 };
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {

		// System.out.println("we have touchdown");
		Vector3 touchPos = new Vector3(screenX, screenY, 0);
		camera.unproject(touchPos);

		if (touchPos.x > 400) {
			// we're on buttons
			if(touchPos.y > 200) {
				//save button butoon
				System.out.println("saving");
				m_game.save();
			}else {
				System.out.println("exiting");
				Gdx.app.exit();
				System.exit(0);
				//exit button
			}
		} else {
			// we're on the board
			if (!m_game.pieceSelected) {// no piece is selected, look if click can select a piece and remember selected
				// piece
				//System.out.println("no piece selected. looking for piece");
				for (int i = 0; i < 8; i++) {
					for (int j = 0; j < 8; j++) {
						if ((spriteBoard[i][j] != null)) {
							if (spriteBoard[i][j].getBoundingRectangle().contains(touchPos.x, touchPos.y)) {
								if (((m_game.currentTurn == m_game.gameBoard[i][j].getColour()))) {

									// System.out.println("piece at: "+i+":"+j + " selected");
									spriteBoard[i][j].isSelected = true;
									m_game.pieceSelected = true;
									m_game.selectedPieceCoords = new int[] { i, j };
									//System.out.println("piece at " + i + ":" + j + "selected");
								} else {
									// System.out.println("colour of piece did not match");
								}

							}

						}

					}

				}
			}
			if (m_game.pieceSelected) {// look to try and move piece
				//System.out.println(
				//		"piece at " + m_game.selectedPieceCoords[0] + ":" + m_game.selectedPieceCoords[1] + "selected");
				int[] target = roundCoords(touchPos.x, touchPos.y);
				int[] arrayTarget = boardToArray(target);
				if (m_game.selectedPieceCoords[0] == arrayTarget[0]
						&& m_game.selectedPieceCoords[1] == arrayTarget[1]) {
					//System.out.println("tried to move to current location. no beuno");
				} else {
					if (m_game.validateMove(m_game.selectedPieceCoords, arrayTarget)) {
						//System.out.println();
						//System.out.println("move was valid under validate move");
						//System.out.println();
						m_game.executeMove(arrayTarget);
					}
					m_game.pieceSelected = false;
					/**
					 * if (spriteBoard[selectedPieceCoords[0]][selectedPieceCoords[1]]
					 * .validMove(new int[] { target[0], target[1] })) { pieceSelected = false;
					 * spriteBoard[selectedPieceCoords[0]][selectedPieceCoords[1]].isSelected =
					 * false;
					 *
					 * movePiece(selectedPieceCoords[0], selectedPieceCoords[1], arrayTarget[0],
					 * arrayTarget[1]); currentTurn = colours.changeColour(currentTurn);
					 * System.out.println("It is now: " + currentTurn + "'s turn"); } pieceSelected
					 * = false;
					 */
					// spriteBoard[selectedPieceCoords[0]][selectedPieceCoords[1]].isSelected =
					// false;
				}
			}
		}

		System.out.println("");
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}
}
