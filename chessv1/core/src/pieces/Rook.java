/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pieces;


import com.arul.chess.*;

/**
 *
 * @author agraru13
 */
public class Rook extends Piece {

    public Rook(colours colour, int[] coords) {
        super(pieces.ROOK, colour, coords);
    }

    @Override
    public boolean isValidMovement(int[] targetCoords) {
        try {
            if (MainChessGUI.m_game.gameBoard[targetCoords[0]][targetCoords[1]].pieceColour == this.pieceColour) {
                return false;
            }
        } catch (NullPointerException e) {
            System.out.println("can move");
        }//s

        if (this.coords[0] == targetCoords[0]) {
            System.out.println("Rook x axis is same");
            int ydir = Math.round(Math.signum(targetCoords[1] - this.coords[1]));
            System.out.println(ydir);
            for (int i = this.coords[1] + ydir; i != targetCoords[1]; i += ydir) {
                if (MainChessGUI.m_game.gameBoard[coords[0]][i] != null) {
                    System.out.println("checking position: "+coords[0] + "," + i);
                    return false;
                }

            }
            return true;

        } else if (this.coords[1] == targetCoords[1]) {
            System.out.println("Rook y axis is same");
            int xdir = Math.round(Math.signum(targetCoords[0] - this.coords[0]));
            System.out.println(xdir);
            for (int i = this.coords[0] + xdir; i != targetCoords[0]; i += xdir) {
                if (MainChessGUI.m_game.gameBoard[i][coords[1]] != null) {
                    System.out.println("checking position: "+coords[0] + "," + i);
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
