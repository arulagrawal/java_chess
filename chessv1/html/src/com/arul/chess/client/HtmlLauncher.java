package com.arul.chess.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.arul.chess.MainChessGUI;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                return new GwtApplicationConfiguration(400, 400);
        }

        @Override
        public ApplicationListener createApplicationListener () {
                return new MainChessGUI();
        }
}