package com.arul.chess;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**@author cbradley */
public class StorageManager {
    private Connection conn;

    public StorageManager(String databaseName) throws ClassNotFoundException, SQLException {
        String driver="net.ucanaccess.jdbc.UcanaccessDriver";
        Class.forName(driver);
        conn=DriverManager.getConnection("jdbc:ucanaccess://"+databaseName);
    }
    
    /**
     * Runs an SQL SELECT query on the database connected and returns the result
     * of the query
     *
     * @param SQL The SQL query
     * @return Returns the query result
     * @throws SQLException Throws an SQL Exception in the event of an error
     * with the query
     */
    public ResultSet query(String SQL) throws SQLException{
        Statement stmt=conn.createStatement();
        ResultSet result=stmt.executeQuery(SQL);
        return result;
    }
    
    /**
     * Runs a SQL UPDATE, DELETE or INSERT query on the database connected
     *
     * @param SQL The SQL query
     * @return either the row count for SQL Data Manipulation Language (DML) statement or 
     * 0 for SQL statements that return nothing
     * @throws SQLException SQLException Throws an SQL Exception in the event of
     * an error with the query
     */
    public int update(String SQL) throws SQLException{
        Statement stmt=conn.createStatement();
        int done=stmt.executeUpdate(SQL);
        return done;
    }
    
    /**
     * Executes an SQL query and then returns any generated key of the record
     * effected by the query
     *
     * @param SQL The SQL statement that is to be run
     * @return Returns the generated key of the record effected (-1 will be
     * returned if the record does not contain a generated key)
     * @throws SQLException SQLException SQLException Throws an SQL Exception in
     * the event of an error in executing the method
     */
    public int updateReturnID(String SQL) throws SQLException{
        Statement stmt=conn.createStatement();
        int id=-1;
        stmt.executeUpdate(SQL, Statement.RETURN_GENERATED_KEYS);
        ResultSet result=stmt.getGeneratedKeys();
        if(result.next()){
            id=result.getInt(1);
        }
        return id;
    }
}