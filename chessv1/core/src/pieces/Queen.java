/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pieces;

import com.arul.chess.MainChessGUI;

/**
 *
 * @author agraru13
 */
public class Queen extends Piece{

    public Queen(colours colour, int[] coords) {
        super(pieces.QUEEN, colour, coords);
    }

    @Override
    public boolean isValidMovement(int[] targetCoords) {
        try {
            if (MainChessGUI.m_game.gameBoard[targetCoords[0]][targetCoords[1]].pieceColour == this.pieceColour) {
                return false;
            }
        } catch (NullPointerException e) {
            System.out.println("can move");
        }     
        
        //check diagonal movement
        int coordDiff = targetCoords[1] - coords[1];
        int ycoeff = 0;
        int xcoeff = 0;
        if (MainChessGUI.m_game.gameBoard[targetCoords[0]][targetCoords[1]] == null || (MainChessGUI.m_game.gameBoard[targetCoords[0]][targetCoords[1]].getColour() != pieceColour)) {
            if (Math.abs(targetCoords[0] - coords[0]) == Math.abs(targetCoords[1] - coords[1])) {
                System.out.println("bishop tried to move:" + (Math.abs(targetCoords[0] - coords[0]) == Math.abs(targetCoords[1] - coords[1])));
                if(targetCoords[1] - coords[1] > 0){ //determine whether the algorithm needs to go up or down on y axis
                    ycoeff = 1;
                }else{
                    ycoeff = -1;
                }
                if(targetCoords[0] - coords[0] > 0){//determine whether the algorithm needs to go left or right on x axis
                    xcoeff = 1;
                }else{
                    xcoeff = -1;
                }
                for (int i = 1; i <= Math.abs(coordDiff) - 1; i++) { //draws path of bishop and checks for pieces in the way
                    //System.out.println("looking at "+ (coords[0] + i*xcoeff) + ":" + (coords[1] + i*ycoeff));
                    if(MainChessGUI.m_game.gameBoard[coords[0] + i*xcoeff][coords[1] + i*ycoeff] != null){//looks at coordinates from original to new position 1 by 1
                        return false;
                    }
                }
                return true;
            }
        }
        //check linear movement
        if (this.coords[0] == targetCoords[0]) {
            System.out.println("Rook x axis is same");
            int ydir = Math.round(Math.signum(targetCoords[1] - this.coords[1]));
            System.out.println(ydir);
            for (int i = this.coords[1] + ydir; i != targetCoords[1]; i += ydir) {
                if (MainChessGUI.m_game.gameBoard[coords[0]][i] != null) {
                    System.out.println("checking position: "+coords[0] + "," + i);
                    return false;
                }

            }
            return true;

        } else if (this.coords[1] == targetCoords[1]) {
            System.out.println("Rook y axis is same");
            int xdir = Math.round(Math.signum(targetCoords[0] - this.coords[0]));
            System.out.println(xdir);
            for (int i = this.coords[0] + xdir; i != targetCoords[0]; i += xdir) {
                if (MainChessGUI.m_game.gameBoard[i][coords[1]] != null) {
                    System.out.println("checking position: "+coords[0] + "," + i);
                    return false;
                }
            }
            return true;
        }
        return false;
        
//        if(this.coords[0] == targetCoords[0] || this.coords[1] == targetCoords[1]){
//            return true;
//        }else{
//            if(Math.abs(targetCoords[0] - coords[0]) == Math.abs(targetCoords[1] - coords[1])){
//                return true;
//            }
//        }
    }
    
}
