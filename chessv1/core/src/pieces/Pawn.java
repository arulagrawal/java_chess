/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pieces;

import com.arul.chess.MainChessGUI;

/**
 *
 * @author agraru13
 */
public class Pawn extends Piece{

    boolean firstMove;

    public Pawn(colours colour, int[] coords){
        super(pieces.PAWN, colour, coords);
        firstMove = true;
    }

    @Override
    public boolean isValidMovement(int[] targetCoords) throws NullPointerException{
        try{
            if(MainChessGUI.m_game.gameBoard[targetCoords[0]][targetCoords[1]].pieceColour == this.pieceColour){
                return false;
            }
        }catch(NullPointerException e) {
            System.out.println("can move");
        }
        boolean targetColour = false;
        if(this.pieceColour == colours.WHITE){
            //System.out.println("current pos:" + this.coords[0] + ";" + this.coords[1]);
            //System.out.println("target pos:" + targetCoords[0] + ";" + targetCoords[1]);
            if(MainChessGUI.m_game.gameBoard[targetCoords[0]][targetCoords[1]] == null){
                targetColour = false;
            }else if(MainChessGUI.m_game.gameBoard[targetCoords[0]][targetCoords[1]].getColour() == colours.BLACK){
                targetColour = true;
            }
            //check diagonal capture
            if(Math.abs(targetCoords[0] - this.coords[0]) == 1 && targetCoords[1] - this.coords[1] == 1){
                if(targetColour){
                    return true;
                }else{
                    return false;
                }
            }
            //check linear movement, assert no capture on linear
            if(targetCoords[0] == this.coords[0]){
                if(firstMove){
                    if(targetCoords[1] == this.coords[1] + 2){
						if(targetColour){
                             return false;
						}
                        return true;
                    }
                }
                if(targetCoords[1] == this.coords[1] + 1){
                    if(targetColour){
                        return false;
                    }
                    return true;
                }
            }

        }else{ //piece is black, can only move down
            //check diag capture
        	System.out.println("black pawn at "+coords[0] + ":" + coords[1]);
        	System.out.println("we know its black okay");
            if(MainChessGUI.m_game.gameBoard[targetCoords[0]][targetCoords[1]] == null){
                targetColour = false;
            }else if(MainChessGUI.m_game.gameBoard[targetCoords[0]][targetCoords[1]].getColour() == colours.WHITE){
                targetColour = true;
            }
            if(Math.abs(targetCoords[0] - this.coords[0]) == 1 && targetCoords[1] - this.coords[1] == -1){
                if(targetColour){
                    return true;
                }else{
                    return false;
                }
            }
            //check linear movement
            if(targetCoords[0] == this.coords[0]){
            	System.out.println("black pawn at "+coords[0] + ":" + coords[1]);
                if(firstMove){
                    if(targetCoords[1] == this.coords[1] - 2){
                        if(targetColour){
                            return false;
                        }
                        return true;
                    }
                }
                if(targetCoords[1] == this.coords[1] - 1){
                	System.out.println("black pawn at "+coords[0] + ":" + coords[1]);
                	System.out.println("diff in y = 1");
                    if(targetColour){
                        return false;
                    }
                    return true;
                }
            }
        }
        /*
         * if(this.pieceColour == colours.WHITE){
         *
         * if (firstMove) { firstMove = false; } if(targetCoords[1] ==
         * this.coords[1] + 1){ if(Math.abs(targetCoords[0] - this.coords[0]) ==
         * 1){ return true; } } }else{ if(targetCoords[1] == this.coords[1] -
         * 1){ if(Math.abs(targetCoords[0] - this.coords[0]) == 1){ return true;
         * } }
        }
         */
        return false;
    }

    /*
     * @Override public boolean movePiece(int[] target) { this.coords[0] =
     * target[0]; this.coords[1] = target[1]; return true;
    }
     */
}
