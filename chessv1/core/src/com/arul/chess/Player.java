/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arul.chess;

import java.sql.ResultSet;

import javax.swing.JOptionPane;

/**
 *
 * @author arul
 */
public class Player {
	private String m_username;
	private String m_password;
	private int m_wins;
	private int m_losses;
	private int m_draws;

	public Player(String user, String pass) {
		m_username = user;
		m_password = pass;
		m_wins = 0;
		m_losses = 0;
		m_draws = 0;
	}

	/**
	 * registers a user to the database
	 */
	public void registerUser() {
		if (!userExists()) {
			try {
				StorageManager stm = new StorageManager("storage.accdb");
				String query = "INSERT INTO tblUsers(Username, [Password]) VALUES('" + m_username + "','" + m_password
						+ "');";
				stm.update(query);
				query = "INSERT INTO tblScore(Username, Wins, Losses, Draws) VALUES('" + m_username + "', 0, 0, 0";
				stm.update(query);
			} catch (Exception e) {
				System.out.println("soz22");
				System.out.println("exception: " + e);
			}
		}else {
			JOptionPane.showMessageDialog(null, "User already exists");
		}

	}

	/**
	 * checks if a given username already exits in the database
	 * @return true if name exists, false if not
	 */
	public boolean userExists() {
		try {
			StorageManager stm = new StorageManager("storage.accdb");
			String query = "SELECT Username FROM tblUsers WHERE Username = '" + m_username + "'";
			ResultSet result = stm.query(query);
			result.next();
			return true;
		} catch (Exception e) {
			System.out.println(e);
			JOptionPane.showMessageDialog(null, "User already exists");
			return false;
		}
	}
	
	/**
	 * validates user credentials
	 * @return - true if valid details entered, false if not
	 */
	public boolean validateLogin() {
		ResultSet result = null;
		try {
			StorageManager stm = new StorageManager("storage.accdb");
			String query = "SELECT * FROM tblUsers WHERE Username = '" + m_username + "' AND [Password] = '"
					+ m_password + "'";
			result = stm.query(query);
			result.next();
			String r = result.getString(1);
			// System.out.println(r);
			// System.out.println();
			if (r.equals(m_username)) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Details not valid");
			System.out.println("soz");
			System.out.println("exception: " + e);
		}
		return false;
	}

	/**
	 * gets this users scores from the database
	 */
	public void getScore() {
		try {
			StorageManager stm = new StorageManager("storage.accdb");
			String query = "SELECT Wins, Losses, Draws FROM tblScore WHERE Username = '" + m_username + "'";
			ResultSet result = stm.query(query);
			result.next();
			m_wins = result.getInt(1);
			m_losses = result.getInt(2);
			m_draws = result.getInt(3);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * increase wins in database
	 */
	public void win() {
		getScore();
		m_wins++;
		updateScore("Wins", m_wins);
	}

	/**
	 * increase losses in database
	 */
	public void lose() {
		getScore();
		m_losses++;
		updateScore("Losses", m_losses);
	}

	/**
	 * increase draws in database
	 */
	public void draw() {
		getScore();
		m_draws++;
		updateScore("Draws", m_draws);
	}

	/**
	 * update scores from above methods
	 * @param score - the field being updated
	 * @param n - the new score
	 */
	public void updateScore(String score, int n) {
		try {
			StorageManager stm = new StorageManager("storage.accdb");
			String query = "UPDATE tblScore SET " + score + " = " + n + " WHERE Username = '" + m_username + "'";
			// System.out.println(query);
			stm.update(query);
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public String getUsername() {
		return m_username;
	}

}
