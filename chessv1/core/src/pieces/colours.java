/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pieces;

/**
 *
 * @author agraru13
 */
public enum colours{
    WHITE, BLACK;

    public static colours changeColour(colours input){
        if(input == colours.WHITE){
            input = colours.BLACK;
        }else{
            input = colours.WHITE;
        }
        return input;
    }

    public colours changeColours(){
        return changeColour(this);
    }
}
