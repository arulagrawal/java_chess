package pieces;

import com.arul.chess.MainChessGUI;

//import chess.colours;
//import chess.pieces;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


abstract public class Piece {
    private pieces thisPiece;
    colours pieceColour;
    //coords are in format x, y;
    int[] coords = new int[2];
    public Piece(pieces piece, colours colour, int[] coords){
        thisPiece = piece;
        pieceColour = colour;
        this.coords = coords;
    }
    
    /**
     * abstract method that checks if a move is valid in its destination as well as path
     * @param targetCoords - the coords the piece is trying to go to
     * @return true = valid, false = invalid
     */
    public abstract boolean isValidMovement(int[] targetCoords);
    
    public boolean movePiece(int[] targetCoords){
        if(isValidMovement(targetCoords)){
            coords = targetCoords;
            
            return true;
        }else{
            return false;
        }
    }
    
    public colours getColour(){
    	return pieceColour;
    }
    
    public pieces getPieceType() {
    	return thisPiece;
    }
	
	public int[] getCoords(){
		return coords;
	}	
}
