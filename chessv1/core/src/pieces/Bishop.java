/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pieces;

import com.arul.chess.MainChessGUI;

/**
 *
 * @author agraru13
 */
public class Bishop extends Piece {

    public Bishop(colours colour, int[] coords) {
        super(pieces.BISHOP, colour, coords);
    }

    @Override
    public boolean isValidMovement(int[] targetCoords) {
        System.out.println("current pos:" + this.coords[0] + ";" + this.coords[1]);
        System.out.println("target pos:" + targetCoords[0] + ";" + targetCoords[1]);
        int coordDiff = targetCoords[1] - coords[1];
        int ycoeff = 0;
        int xcoeff = 0;
        if ((MainChessGUI.m_game.gameBoard[targetCoords[0]][targetCoords[1]] == null) || (MainChessGUI.m_game.gameBoard[targetCoords[0]][targetCoords[1]].getColour() != pieceColour)) {
            if (Math.abs(targetCoords[0] - coords[0]) == Math.abs(targetCoords[1] - coords[1])) {
                System.out.println("bishop tried to move:" + (Math.abs(targetCoords[0] - coords[0]) == Math.abs(targetCoords[1] - coords[1])));
                if(targetCoords[1] - coords[1] > 0){ //determine whether the algorithm needs to go up or down on y axis
                    ycoeff = 1;
                }else{
                    ycoeff = -1;
                }
                if(targetCoords[0] - coords[0] > 0){//determine whether the algorithm needs to go left or right on x axis
                    xcoeff = 1;
                }else{
                    xcoeff = -1;
                }
                for (int i = 1; i <= Math.abs(coordDiff) - 1; i++) { //draws path of bishop and checks for pieces in the way
                    //System.out.println("looking at "+ (coords[0] + i*xcoeff) + ":" + (coords[1] + i*ycoeff));
                    if(MainChessGUI.m_game.gameBoard[coords[0] + i*xcoeff][coords[1] + i*ycoeff] != null){//looks at coordinates from original to new position 1 by 1
                        return false;
                    }
                }
                return true;
            }
            return false;
        } else {
            return false;
        }
    }
}
